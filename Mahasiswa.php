<?php
class Mahasiswa extends dbConector{
    public $id;
    public $nim;
    public $nama;

    private $tablename;

    function __construct(){
        $this->tablename = "mahasiswa";
        parent::__construct();
    }

    public function getAll(){
        $result = $this->db->query("select * 
        from $this->tablename");
        return $result->fetchAll();
    }
}
?>